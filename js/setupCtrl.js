'use strict';

module.exports = [
    '$scope',
    'Controls',
    function ($scope, Controls) {
        $scope.interval = Controls.interval;
        $scope.setType = function (type) {
            Controls.interval = $scope.interval = type;
            Controls.doUpdate();
        };

        $scope.series = [
            'Yield',
            'Spread',
            'Price'
        ];

        $scope.selectedSerie = Controls.series;
        $scope.setSerie = function () {
            Controls.series = $scope.selectedSerie;
            Controls.doUpdate();
        };
    }
];