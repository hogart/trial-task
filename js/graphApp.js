'use strict';

var angular = require('angular');
var graphApp = angular.module('graphApp', []);

graphApp.factory('Controls', function () {
    return {
        interval: 'week',
        series: 'yield',
        _cb: null,
        onUpdate: function (cb) {
            this._cb = cb;
        },
        doUpdate: function () {
            this._cb();
        }
    };
});

graphApp.controller('setupCtrl', require('./setupCtrl'));
graphApp.controller('chartCtrl', require('./chartCtrl'));

module.exports = graphApp;