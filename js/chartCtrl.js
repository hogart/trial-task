'use strict';

var d3 = require('d3');
var series = require('./series');

var height = 400;
var width = 900;
var margin = {
    left: 50,
    right: 50,
    top: 50,
    bottom: 50
};

module.exports = [
    '$scope',
    '$element',
    'Controls',
    function ($scope, $element, Controls) {
        var x = d3.time.scale().range([0, width]);
        var y = d3.scale.linear().range([height, 0]);
        var xAxis = d3.svg.axis().scale(x).tickSize(-height).tickSubdivide(true);
        var yAxis = d3.svg.axis().scale(y).ticks(4).orient('right');

        var line = d3.svg.line()
            .interpolate('monotone')
            .x(function(d) { return x(d.date); })
            .y(function(d) { return y(d[Controls.series]); });

        x.domain([series[Controls.interval][0].date, series[Controls.interval][series[Controls.interval].length - 1].date]);
        y.domain([0, d3.max(series[Controls.interval], function(d) { return d[Controls.series]; })]).nice();

        // Add an SVG element with the desired dimensions and margin.
        var svg = d3.select($element[0]).append('svg')
            .attr('width', width + margin.left + margin.right)
            .attr('height', height + margin.top + margin.bottom)
            .append('g')
            .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

        svg.append('clipPath')
            .attr('id', 'clip')
            .append('rect')
            .attr('width', width)
            .attr('height', height);

        // Add the x-axis.
        svg.append('g')
            .attr('class', 'x axis')
            .attr('transform', 'translate(0,' + height + ')')
            .call(xAxis);

        // Add the y-axis.
        svg.append('g')
            .attr('class', 'y axis')
            .attr('transform', 'translate(' + width + ',0)')
            .call(yAxis);

        // Add the line path.
        svg.append('path')
            .attr('class', 'line')
            .attr('clip-path', 'url(#clip)')
            .attr('d', line(series[Controls.interval]));

        function updateChart () {
            x.domain([series[Controls.interval][0].date, series[Controls.interval][series[Controls.interval].length - 1].date]);
            y.domain([0, d3.max(series[Controls.interval], function(d) { return d[Controls.series]; })]).nice();
            var t = svg.transition().duration(250);
            t.select('.x.axis').call(xAxis);
            t.select('.y.axis').call(yAxis);
            t.select('.line').attr('d', line(series[Controls.interval]));
        }

        Controls.onUpdate(updateChart);
    }
];