'use strict';

var ms = require('ms');

var today = new Date();
var week = ms('7d');
var weekAgo = new Date(today - week).getTime();
var monthAgo = new Date(today).setMonth(today.getMonth() - 1);
var quarterAgo = new Date(today).setMonth(today.getMonth() - 3);
var yearAgo = new Date(today).setFullYear(today.getFullYear() - 3);

var series = {
    week: [],
    month: [],
    quarter: [],
    year: []
};

function randValue (min, max) {
    return min + Math.random() * (max - min);
}

for (var i = 0; i < 6; i++) {
    series.week.push({
        date: weekAgo + ms('1d') * i,
        yield: randValue(30, 40),
        spread: randValue(30, 40),
        price: randValue(30, 40)
    });
}

for (i = 0; i < 4; i++) {
    series.month.push({
        date: monthAgo + week * i,
        yield: randValue(29, 43),
        spread: randValue(29, 43),
        price: randValue(29, 43)
    });
}

for (i = 0; i < 12; i++) {
    series.quarter.push({
        date: quarterAgo + week * i,
        yield: randValue(27, 44),
        spread: randValue(27, 44),
        price: randValue(27, 44)
    });
}

for (i = 0; i < 12; i++) {
    series.year.push({
        date: yearAgo + ms('30d') * i,
        yield: randValue(27, 46),
        spread: randValue(27, 46),
        price: randValue(27, 46)
    });
}

module.exports = series;